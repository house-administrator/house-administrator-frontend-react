import React from 'react';
import { Route, Switch } from 'react-router-dom';
// add components
import App from '../App';
import Navbar from '../components/navbar/Navbar';
import Dashboard from '../components/dashboard/Dashboard';

const AppRoutes = () => (
	<App>
		<Switch>
			<Route exact path="/Navbar" component={Navbar} />
			<Route exact path="/Dashboard" component={Dashboard} />
		</Switch>
	</App>
);

export default AppRoutes;
